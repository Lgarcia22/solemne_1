/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.model;

/**
 *
 * @author luisg
 */
public class Calculadora {
     private double isimple;
   private int capital;
   private float interesa;
   private int numanos;

    

    public Calculadora(int capitalInt, float interesaInt, int numanosInt) {
         this.capital = capital;
        this.interesa = interesa;
        this.numanos = numanos;
    }

  
   

    public double getIsimple() {
        return isimple;
    }

    public void setIsimple(double isimple) {
        this.isimple = isimple;
    }

    public int getCapital() {
        return capital;
    }

    public void setCapital(int capital) {
        this.capital = capital;
    }

    public float getInteresa() {
        return interesa;
    }

    public void setInteresa(float interesa) {
        this.interesa = interesa;
    }

    public int getNumanos() {
        return numanos;
    }

    public void setNumanos(int numanos) {
        this.numanos = numanos;
    }
   
   public void calcularInteres(){
       
      this.isimple= this.getCapital() * (this.getInteresa() / 100) * this.getNumanos();
   }
}
