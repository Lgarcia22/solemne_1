<%-- 
    Document   : resultado
    Created on : 25-04-2021, 17:35:38
    Author     : luisg
--%>

<%@page import="cl.model.Calculadora"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
    Calculadora cal=(Calculadora) request.getAttribute("Calculadora");
    %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Calcular interés simple</title>
    </head>
    <body>
        <h1>El resultado del interés simple es: <%= cal.getIsimple()%></h1>
    </body>
</html>
